/**
 * Created by Counter on 7/1/2017.
 */
/**
 * Created by Counter on 7/1/2017.
 */
import React,{Component} from 'react'
import Header from '../Header/header'
import './home.css'
import * as actions from '../../actions/authActions'
import {connect} from 'react-redux'
import 'aframe';
import 'aframe-animation-component';
import 'aframe-particle-system-component';
import 'babel-polyfill';
import {Entity, Scene} from 'aframe-react';
import Camera from '../presentational/Camera/camera'
import Cursor from '../presentational/Cursor/cursor'


export class Home extends Component{
    constructor(props){
        super(props);
        if(!localStorage.getItem('key')){
            console.log('key')
            this.props.history.push('/')
        }
        this.state = {
            imgData:[],
            vrModeButton:false
        };
        this.openInVr = this.openInVr.bind(this)
        this.VrModeComponentopen = this.VrModeComponentopen.bind(this)
        this.handleClick = this.handleClick.bind(this)


    }

    componentDidMount(){
       this.props.getAll360Images()
        let btn = JSON.parse(localStorage.getItem("vrModeButton"))
        if(btn !== true){
            this.setState({
                vrModeButton:false
            })
        }else{
            this.setState({
                vrModeButton:true
            })
        }
    }

    handleClick = () => {

        localStorage.removeItem('vrModeButton');
        this.setState({
            vrModeButton:false
        })
    };

    handleCollide = () => {
        console.log('Collided!');
    };

    componentWillReceiveProps(newProps){

        this.setState({
            imgData:newProps.imageData
        })
    }
    VrModeComponentopen(){
        localStorage.setItem("vrModeButton",JSON.stringify(true))
        this.setState({
            vrModeButton:true
        })
    }

    componentWillUnmount(){

    }

    openInVr(val){
        localStorage.setItem("vrData",JSON.stringify(val))
        this.props.history.push('/vrmodeOn')
    }

    render(){


        return(
            <div>
                {this.state.vrModeButton?
                <div>
                    <Scene>
                        <Camera><Cursor/></Camera>
                        <Entity  layout={{layout: 'plane' , margin: 2}} position={{x:-3 ,y:-1 ,Z:5}}>
                            <Entity primitive='a-box' color="red" position="3 3 -5" events={{
                                click: this.handleClick,
                                collided: [this.handleCollide]}}/>
                            {(this.state.imgData.length!==0)?
                                this.state.imgData.map((data,i)=>(
                                <Entity key={data.name}
                                geometry={{primitive: 'box', width: 5}}
                                material={{ roughness: 0.5, src: data.pano}}
                                scale={{x: 2, y: 2, z: 2}}
                                position={{x:i*10, y:0 , z:-5  }}
                                        events={{
                                            click: [()=>this.openInVr(data)],
                                            collided: [this.handleCollide]}}/>
                                )):null
                            }
                        </Entity>
                    </Scene>
                </div>:
                <div>
                    <Header logout={this.props.logout}/>
                    <div className="case-study-gallery">
                        <button className="btn_vr" onClick={()=>this.VrModeComponentopen()}>Enter in virtual mode</button>
                        <h1 className="title">VReality</h1>
                        <p> Virtual reality is like dreaming with your eyes open. </p>
                        {
                            (this.state.imgData.length!==0)?
                                this.state.imgData.map((img, i) => (
                                    <div key={i} className="case-study study1">
                                        <figure>
                                            <img className="case-study__img" src={img.pano} alt="" />
                                        </figure>
                                        <div className="case-study__overlay">
                                            <h2 className="case-study__title">{img.name}</h2>
                                            <a className="case-study__link" onClick={()=>this.openInVr(img)}>View in VR</a>
                                        </div>
                                    </div>
                                )):null
                        }
                    </div>
                </div>}

            </div>

        )
    }
}
const mapStateToProps=(state)=>{


    return {
        isAuthenticated:state.auth.isAuthenticated,
        imageData:state.auth.imageData
    }
}


export default connect(mapStateToProps,actions)(Home);
