/**
 * Created by consultadd on 16/7/17.
 */
/**
 * Created by Counter on 7/1/2017.
 */
/**
 * Created by Counter on 7/1/2017.
 */
import React,{Component} from 'react'
import './vrmode.css'
import 'aframe';
import 'aframe-animation-component';
import 'aframe-particle-system-component';
import 'babel-polyfill';
import Header from '../Header/header'
import {Entity, Scene} from 'aframe-react';


class VrMode extends Component{

    constructor(props){
        super(props);
        this.state = {
            vrdata:{}
        };
    }

    componentDidMount(){
        let vrmodedata = JSON.parse(localStorage.getItem("vrData"))

        this.setState({
            vrdata:vrmodedata
        })
    }

    componentWillReceiveProps(newProps){

    }


    render(){

        return(
            <div>
                <Header logout={this.props.logout}/>
                <Scene>
                    <a-sky src={this.state.vrdata.pano} rotation="0 -130 0"></a-sky>
                    <a-text font="kelsonsans" value={this.state.vrdata.name} width="6" position="-2.5 0.5 -1.5"
                            rotation="0 15 0"></a-text>
                </Scene>
            </div>

        )
    }
}


export default VrMode;
