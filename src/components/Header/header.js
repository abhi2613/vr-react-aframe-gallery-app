
import React,{Component} from 'react'
import {Link} from 'react-router-dom'
import './header.css'

class Header extends Component{
    render(){
        return(
            <header className="header">
                <ul>
                    <li> <Link to="/home">Home</Link></li>
                    <li><i className="fa fa-user fa-lg" aria-hidden="true"></i>
                        <ul className="logOutUl">
                            <li className="logOutLi" onClick={this.props.logout}>Logout</li>
                        </ul>
                    </li>
                </ul>
            </header>

        )
    }
}

export default Header

